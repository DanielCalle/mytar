#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "mytar.h"

extern char *use;

/** Copy nBytes bytes from the origin file to the destination file.
 *
 * origin: pointer to the FILE descriptor associated with the origin file
 * destination:  pointer to the FILE descriptor associated with the destination file
 * nBytes: number of bytes to copy
 *
 * Returns the number of bytes actually copied or -1 if an error occured.
 */
int copynFile(FILE* origin, FILE* destination, int nBytes)
{
    int r = 0;
    int reads;

    while( (reads = getc(origin)) != EOF && (r < nBytes) )
    {

        if( fwrite(&reads,sizeof(char),1,destination)==0 )
            return -1;

        r++;

    }

    return r;

}
/** Loads a string from a file.
 *
 * file: pointer to the FILE descriptor 
 * 
 * The loadstr() function must allocate memory from the heap to store 
 * the contents of the string read from the FILE. 
 * Once the string has been properly built in memory, the function returns
 * the starting address of the string (pointer returned by malloc()) 
 * 
 * Returns: !=NULL if success, NULL if error
 */
char*
loadstr(FILE * file)
{
	// Complete the function
	int strlength = 1;
	char c;
	while((c = getc(file)) != '\0' && c != EOF)
		++strlength;

	if(c==EOF)
		return NULL;
	else {

		fseek(file,-(strlength),SEEK_CUR);
		char * str = malloc(sizeof(char)*strlength);

		fread(str,sizeof(char),strlength,file);
		return str;
	}

}

/** Read tarball header and store it in memory.
 *
 * tarFile: pointer to the tarball's FILE descriptor 
 * nFiles: output parameter. Used to return the number 
 * of files stored in the tarball archive (first 4 bytes of the header).
 *
 * On success it returns the starting memory address of an array that stores 
 * the (name,size) pairs read from the tar file. Upon failure, the function returns NULL.
 */
stHeaderEntry*
readHeader(FILE * tarFile, int *nFiles)
{
	// Complete the function
	return NULL;
}

/** Creates a tarball archive 
 *
 * nfiles: number of files to be stored in the tarball
 * filenames: array with the path names of the files to be included in the tarball
 * tarname: name of the tarball archive
 * 
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First reserve room in the file to store the tarball header.
 * Move the file's position indicator to the data section (skip the header)
 * and dump the contents of the source files (one by one) in the tarball archive. 
 * At the same time, build the representation of the tarball header in memory.
 * Finally, rewind the file's position indicator, write the number of files as well as 
 * the (file name,file size) pairs in the tar archive.
 *
 * Important reminder: to calculate the room needed for the header, a simple sizeof 
 * of stHeaderEntry will not work. Bear in mind that, on disk, file names found in (name,size) 
 * pairs occupy strlen(name)+1 bytes.
 *
 */
int
createTar(int nFiles, char *fileNames[], char tarName[])
{
    // reserve room in the file to store the tarball header
	stHeaderEntry *stHeader;
    stHeader = malloc(sizeof(stHeaderEntry)*nFiles);
    int stHeaderSize = sizeof(int) + sizeof(unsigned int)*nFiles;
    for(int i = 0; i<nFiles; ++i)
        stHeaderSize += strlen(fileNames[i]) + 1;
    // Move the file's position indicator to the data section (skip the header)
    FILE * tarFile = fopen(tarName, "w");
    fseek(tarFile,stHeaderSize,SEEK_SET);
    // dump the contents of the source files (one by one) in the tarball archive
    FILE * fileToRead;
    int bytesCopied;
    int fileSize = 2147483647;
    for(int i = 0; i<nFiles; ++i)
    {
        fileToRead = fopen(fileNames[i], "r");
        bytesCopied = copynFile(fileToRead, tarFile, fileSize);
        if(bytesCopied==-1)
            return EXIT_FAILURE;

        stHeader[i].name = malloc(sizeof(char)*strlen(fileNames[i]));
        strcpy(stHeader[i].name, fileNames[i]);
        stHeader[i].size = bytesCopied;

        fclose(fileToRead);

    }
    // rewind the file's position indicator
    fseek(tarFile,0,SEEK_SET);
    fwrite(&nFiles,sizeof(int),1,tarFile);
    //  write the header
    for(int i = 0; i<nFiles; ++i)
    {
        fwrite(stHeader[i].name,sizeof(char),strlen(stHeader[i].name)+1,tarFile);
        fwrite(&stHeader[i].size,sizeof(unsigned int),1,tarFile);
        free(stHeader[i].name);
    }
    free(stHeader);
    fclose(tarFile);
	return EXIT_SUCCESS;
}

/** Extract files stored in a tarball archive
 *
 * tarName: tarball's pathname
 *
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First load the tarball's header into memory.
 * After reading the header, the file position indicator will be located at the 
 * tarball's data section. By using information from the 
 * header --number of files and (file name, file size) pairs--, extract files 
 * stored in the data section of the tarball.
 *
 */
int
extractTar(char tarName[])
{
	// Complete the function
	return EXIT_FAILURE;
}
